package com.mvp.cartoes.cartao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteConfiguration.class)
public interface ClienteServiceClient {

    @GetMapping("/cliente/{id}")
    ClientDto getById(@PathVariable Long id);
}
