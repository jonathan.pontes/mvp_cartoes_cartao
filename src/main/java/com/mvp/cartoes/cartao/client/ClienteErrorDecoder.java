package com.mvp.cartoes.cartao.client;

import com.mvp.cartoes.cartao.utils.exceptions.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.hibernate.ObjectNotFoundException;

public class ClienteErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
