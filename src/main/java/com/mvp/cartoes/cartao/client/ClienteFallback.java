package com.mvp.cartoes.cartao.client;

import java.io.IOException;

public class ClienteFallback implements ClienteServiceClient{

    private Exception cause;

    ClienteFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClientDto getById(Long id) {
        if(cause instanceof IOException) {
            throw new RuntimeException("O serviço de cliente está offline");
        }

        // Cliente fake
        ClientDto clientDto = new ClientDto();
        clientDto.setId(-1L);
        clientDto.setName(cause.getClass().getName());
        return clientDto;
    }

}
