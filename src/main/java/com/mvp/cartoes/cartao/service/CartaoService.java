package com.mvp.cartoes.cartao.service;

import com.mvp.cartoes.cartao.client.ClientDto;
import com.mvp.cartoes.cartao.client.ClienteServiceClient;
import com.mvp.cartoes.cartao.models.Cartao;
import com.mvp.cartoes.cartao.repository.CartaoRepository;
import com.mvp.cartoes.cartao.utils.exceptions.CartaoNotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteServiceClient clientService;

    public Cartao create(Cartao cartao) {

        ClientDto clientDto = clientService.getById(cartao.getClientId());
        cartao.setClientId(clientDto.getId());

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new RuntimeException("Cartão já existente com esse número!");
        }

        // Regras de negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}
