package com.mvp.cartoes.cartao.repository;


import com.mvp.cartoes.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {

    Optional<Cartao> findByNumero(String numero);

}
