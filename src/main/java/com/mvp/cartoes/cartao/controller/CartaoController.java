package com.mvp.cartoes.cartao.controller;

import com.mvp.cartoes.cartao.models.Cartao;
import com.mvp.cartoes.cartao.models.dto.create.CreateCartaoRequest;
import com.mvp.cartoes.cartao.models.dto.create.CreateCartaoResponse;
import com.mvp.cartoes.cartao.models.dto.get.GetCartaoResponse;
import com.mvp.cartoes.cartao.models.dto.update.UpdateCartaoRequest;
import com.mvp.cartoes.cartao.models.dto.update.UpdateCartaoResponse;
import com.mvp.cartoes.cartao.models.mapper.CartaoMapper;
import com.mvp.cartoes.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {

        Cartao cartao = CartaoMapper.fromCreateRequest(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return CartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        Cartao cartao = CartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.update(cartao);
        return CartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);

        return CartaoMapper.toGetResponse(byNumero);
    }

    @GetMapping("/id/{id}")
    public GetCartaoResponse getByNumero(@PathVariable Long id) {
        System.out.println("Buscaram um cartao de id " + id + " em " + System.currentTimeMillis());
        Cartao byNumero = cartaoService.getById(id);

        return CartaoMapper.toGetResponse(byNumero);
    }



}
