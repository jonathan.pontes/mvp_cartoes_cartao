package com.mvp.cartoes.cartao.models.mapper;


import com.mvp.cartoes.cartao.models.Cartao;
import com.mvp.cartoes.cartao.models.dto.create.CreateCartaoRequest;
import com.mvp.cartoes.cartao.models.dto.create.CreateCartaoResponse;
import com.mvp.cartoes.cartao.models.dto.get.GetCartaoResponse;
import com.mvp.cartoes.cartao.models.dto.update.UpdateCartaoRequest;
import com.mvp.cartoes.cartao.models.dto.update.UpdateCartaoResponse;
import com.mvp.cartoes.cartao.client.ClientDto;

public class CartaoMapper {

    public static Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        ClientDto clientDto = new ClientDto();
        clientDto.setId(cartaoCreateRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());

        cartao.setClientId(clientDto.getId());
        return cartao;
    }

    public static CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getClientId());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClientId());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClientId());

        return getCartaoResponse;
    }

}
